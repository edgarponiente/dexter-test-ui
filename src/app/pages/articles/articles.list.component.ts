import {
    Component,
    OnInit,
    ViewChild
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { CanActivate, Router } from '@angular/router';
import { ArticleService } from './articles.service';
import { Article } from './article';
import { AuthService } from '../../services/auth.service';

@Component({
    /**
     * The selector is what angular internally uses
     * for `document.querySelectorAll(selector)` in our index.html
     * where, in this case, selector is the string 'home'.
     */
    // selector: 'home',  // <home></home>
    /**
     * We need to tell Angular's Dependency Injection which providers are in our app.
     */
    providers: [
        ArticleService,
        AuthService
    ],
    /**
     * Our list of styles in our component. We may add more to compose many styles together.
     */
    styleUrls: ['./articles.component.css'],
    /**
     * Every Angular template is first compiled by the browser before Angular runs it's compiler.
     */
    templateUrl: './articles.list.component.html'
})
export class ArticlesListComponent implements OnInit {

    public error: any;
    public user: any;
    public statuses: any;
    public currentStatus: any;
    public articles: Article[];

    /**
     * TypeScript public modifiers
     */
    constructor(
        private router: Router,
        private articleService: ArticleService,
        private authService: AuthService
    ) { }

    public ngOnInit() {

        this.user = JSON.parse(localStorage.getItem('currentUser'));

        this.initArticles();
    }

    initArticles() {
        this.articleService
        .all()
        .subscribe((articles) => {
            this.articles = articles;
        }, (err) => {
            this.error = err;
        });
    }

    /**
     *
     * Delete the article
     *
     * @param articleId Article PK
     */
    public deleteArticle(articleId: number) {
        this.articleService
            .delete(articleId)
            .subscribe((article) => {
                if(article.status) {
                    this.initArticles();
                }
            });
    }

}
