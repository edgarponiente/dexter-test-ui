export class Article {
    public id: number;
    public title: string;
    public short_desc: string;
    public description: string;
    public author: string;
    public category_id: number;
}
