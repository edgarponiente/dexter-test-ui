import {
    Component,
    OnInit,
    ViewChild
} from '@angular/core';
import { NgForm, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { ArticleService } from './articles.service';
import { Article } from './article';
import { AuthService } from '../../services/auth.service';
import { ErrorService } from '../../services/error.service';

@Component({
    /**
     * The selector is what angular internally uses
     * for `document.querySelectorAll(selector)` in our index.html
     * where, in this case, selector is the string 'home'.
     */
    // selector: 'home',  // <home></home>
    /**
     * We need to tell Angular's Dependency Injection which providers are in our app.
     */
    providers: [
        ArticleService,
        ErrorService,
        AuthService,
    ],
    /**
     * Our list of styles in our component. We may add more to compose many styles together.
     */
    styleUrls: ['./articles.component.css'],
    /**
     * Every Angular template is first compiled by the browser before Angular runs it's compiler.
     */
    templateUrl: './articles.edit.component.html'
})
export class ArticlesEditComponent implements OnInit {

    public articleForm: FormGroup;
    public categoryForm: FormGroup;
    public error: any = [];
    public successMessage: string;
    public statuses: any;
    public openCategoryToggle: boolean;
    public article: any;

    /**
     * TypeScript public modifiers
     */
    constructor(
        private router: Router,
        private fb: FormBuilder,
        private articleService: ArticleService,
        private errorService: ErrorService,
        private route: ActivatedRoute
    ) { }

    public ngOnInit() {
        this.route.params.subscribe((params) => {
            let id = params['id'];

            this.articleService
                .find(id)
                .subscribe((article) => {
                    this.article = article;
                    this.createForm(article);
                });
        });

        this.createCategoryForm();

        this.initCategories();
    }

    createForm(article: any) {
        this.articleForm = this.fb.group({
            title: [article.title, Validators.required],
            short_desc: [article.short_desc, Validators.required],
            description: [article.description, Validators.required],
            author: [article.author, Validators.required],
            category_id: [article.category_id, Validators.required]
        });
    }

    initCategories() {
        this.articleService
            .allCategories()
            .subscribe((response) => {
                this.statuses = response;
            });
    }

    createCategoryForm() {
        this.categoryForm = this.fb.group({
            name: ['', Validators.required]
        });
    }

    submit() {
        if (this.articleForm.valid) {
            this.articleService
                .update(this.articleForm.value, this.article.id)
                .subscribe((result) => {
                    this.successMessage = 'Successful';
                }, (err) => {
                    this.error = [];
                    this.error = this.errorService.getErrors(err.errors);
                });
        }
    }

    submitCategory() {
        if (this.categoryForm.valid) {
            this.articleService
                .addCategory(this.categoryForm.value)
                .subscribe((result) => {
                    this.successMessage = 'Successful';
                    this.initCategories();
                    window.scrollTo(0, 0);
                }, (err) => {
                    this.error = [];
                    this.error = this.errorService.getErrors(err.errors);
                    window.scrollTo(0, 0);
                });
        }
    }

    openCategory() {
        this.openCategoryToggle = !this.openCategoryToggle;
    }

}
