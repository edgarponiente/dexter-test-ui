import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { Article } from './article';
import { environment } from '../../environment/env';

@Injectable()
export class ArticleService {

    public user: any;
    public server: string;

    private headers: any;

    constructor(private http: Http) {
        let token = localStorage.getItem('token');

        this.server = environment.server;

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.headers = headers;
        this.user = JSON.parse(localStorage.getItem('currentUser'));
    }

    public all() {
        let headers = this.headers;
        return this.http
            .get(
                this.server + '/api/v1/articles', { headers }
            )
            .map((res) => res.json())
            .catch((err) =>  {
                return Observable.throw(err.json());
            });
    }

    public find(id: number) {

        let headers = this.headers;

        return this.http
            .get(
                this.server + '/api/v1/articles/' + id, { headers }
            )
            .map((res) => res.json())
            .catch((err) =>  {
                return Observable.throw(err.json());
            });
    }

    public add(article: any) {

        let headers = this.headers;

        return this.http
            .post(
                this.server + '/api/v1/articles', JSON.stringify(article), { headers }
            )
            .map((res) => res.json())
            .catch((err) =>  {
                return Observable.throw(err.json());
            });
    }

    public update(article: Article, id: number) {

        let headers = this.headers;

        return this.http
            .put(
                this.server + '/api/v1/articles/' + id, JSON.stringify(article), { headers }
            )
            .map((res) => res.json())
            .catch((err) =>  {
                return Observable.throw(err.json());
            });
    }

    public delete(id: number) {

        let headers = this.headers;

        return this.http
            .delete(
                this.server + '/api/v1/articles/' + id, { headers }
            )
            .map((res) => res.json())
            .catch((err) =>  {
                return Observable.throw(err.json());
            });
    }

    public allCategories() {
        let headers = this.headers;
        return this.http
            .get(
                this.server + '/api/v1/categories', { headers }
            )
            .map((res) => res.json())
            .catch((err) =>  {
                return Observable.throw(err.json());
            });
    }

    public addCategory(category: any) {

        let headers = this.headers;

        return this.http
            .post(
                this.server + '/api/v1/categories', JSON.stringify(category), { headers }
            )
            .map((res) => res.json())
            .catch((err) =>  {
                return Observable.throw(err.json());
            });
    }
}
