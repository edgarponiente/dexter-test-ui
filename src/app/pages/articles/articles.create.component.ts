import {
    Component,
    OnInit,
    ViewChild
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ArticleService } from './articles.service';
import { AuthService } from '../../services/auth.service';
import { ErrorService } from '../../services/error.service';

@Component({
    /**
     * The selector is what angular internally uses
     * for `document.querySelectorAll(selector)` in our index.html
     * where, in this case, selector is the string 'home'.
     */
    // selector: 'home',  // <home></home>
    /**
     * We need to tell Angular's Dependency Injection which providers are in our app.
     */
    providers: [
        ArticleService,
        ErrorService,
        AuthService,
    ],
    /**
     * Our list of styles in our component. We may add more to compose many styles together.
     */
    styleUrls: ['./articles.component.css'],
    /**
     * Every Angular template is first compiled by the browser before Angular runs it's compiler.
     */
    templateUrl: './articles.create.component.html'
})
export class ArticlesCreateComponent implements OnInit {

    public articleForm: FormGroup;
    public categoryForm: FormGroup;
    public error: any = [];
    public successMessage: string;
    public statuses: any;
    public openCategoryToggle: boolean;

    /**
     * TypeScript public modifiers
     */
    constructor(
        private router: Router,
        private fb: FormBuilder,
        private articleService: ArticleService,
        private errorService: ErrorService
    ) { }

    public ngOnInit() {
        this.createForm();

        this.createCategoryForm();

        this.initCategories();
    }

    createForm() {
        this.articleForm = this.fb.group({
            title: ['', Validators.required],
            short_desc: ['', Validators.required],
            description: ['', Validators.required],
            author: ['', Validators.required],
            category_id: ['', Validators.required]
        });
    }

    initCategories() {
        this.articleService
            .allCategories()
            .subscribe((response) => {
                this.statuses = response;
            });
    }

    createCategoryForm() {
        this.categoryForm = this.fb.group({
            name: ['', Validators.required]
        });
    }

    submit() {
        if (this.articleForm.valid) {
            this.articleService
                .add(this.articleForm.value)
                .subscribe((result) => {
                    this.successMessage = 'Successful';
                }, (err) => {
                    this.error = [];
                    this.error = this.errorService.getErrors(err.errors);
                });
        }
    }

    submitCategory() {
        if (this.categoryForm.valid) {
            this.articleService
                .addCategory(this.categoryForm.value)
                .subscribe((result) => {
                    this.successMessage = 'Successful';
                    this.initCategories();
                    window.scrollTo(0, 0);
                }, (err) => {
                    this.error = [];
                    this.error = this.errorService.getErrors(err.errors);
                    window.scrollTo(0, 0);
                });
        }
    }

    openCategory() {
        this.openCategoryToggle = !this.openCategoryToggle;
    }

}
