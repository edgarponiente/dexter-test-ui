import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { ArticlesListComponent } from './articles.list.component';
import { ArticlesCreateComponent } from './articles.create.component';
import { ArticlesEditComponent } from './articles.edit.component';
import { ArticlesDetailComponent } from './articles.detail.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: ArticlesListComponent
  },
  {
    path: 'create',
    component: ArticlesCreateComponent
  },
  {
    path: 'edit/:id',
    component: ArticlesEditComponent
  },
  {
    path: ':id',
    component: ArticlesDetailComponent
  }
];

@NgModule({
  declarations: [
    ArticlesListComponent,
    ArticlesDetailComponent,
    ArticlesCreateComponent,
    ArticlesEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(ROUTES),
  ],
  // ...
})
export class ArticlesModule {}
