import {
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';


@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  // selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
    AuthService
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styles: [],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public errorMessage: boolean = false;
  private loggedIn = false;

  /**
   * TypeScript public modifiers
   */
  constructor(
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder
  ) { }

  public ngOnInit() {
    this.loggedIn = !!localStorage.getItem('token');

    this.loginForm = this.fb.group({
      'password': new FormControl('', Validators.required)
    });
  }

  submitLogin()
  {
    let form = this.loginForm.value;

    this.authService
      .login(form.password)
      .subscribe((res) => {
        if(res.status === false) {
          this.errorMessage = true;
        } else {
          localStorage.setItem('token', res.token);
          this.router.navigate(['/articles']);
        }
      });
  }
}
