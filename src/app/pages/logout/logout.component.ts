import {
  Component,
  OnInit
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  // selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
    AuthService
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styles: [],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  template: ''
})
export class LogoutComponent implements OnInit {

  /**
   * TypeScript public modifiers
   */
  constructor(
      private router: Router,
      private authService: AuthService
  ) {}

  public ngOnInit() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
