import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogoutComponent } from './pages/logout/logout.component';
import { LoginComponent } from './pages/login/login.component';
import { ArticlesListComponent } from './pages/articles/articles.list.component';
import { ArticlesCreateComponent } from './pages/articles/articles.create.component';
import { ArticlesDetailComponent } from './pages/articles/articles.detail.component';
import { ArticlesEditComponent } from './pages/articles/articles.edit.component';
import { LoggedInGuard } from './middlewares/logged-in-guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  {
    path: 'articles',
    children: [
      {
        path: '',
        component: ArticlesListComponent
      },
      {
        path: 'create',
        component: ArticlesCreateComponent
      },
      {
        path: ':id/edit',
        component: ArticlesEditComponent
      },
      {
        path: ':id',
        component: ArticlesDetailComponent
      }
    ],
    canActivate: [LoggedInGuard]
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
