import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './pages/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { LogoutComponent } from './pages/logout/logout.component';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ArticlesListComponent } from './pages/articles/articles.list.component';
import { ErrorService } from './services/error.service';
import { SharedComponent } from './partials/shared.component';
import { ArticlesCreateComponent } from './pages/articles/articles.create.component';
import { ArticlesEditComponent } from './pages/articles/articles.edit.component';
import { ArticlesDetailComponent } from './pages/articles/articles.detail.component';
import { LoggedInGuard } from './middlewares/logged-in-guard';


@NgModule({
  imports:      [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    ReactiveFormsModule,
    SharedComponent
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    ArticlesListComponent,
    ArticlesCreateComponent,
    ArticlesEditComponent,
    ArticlesDetailComponent
   ],
  providers: [
    AuthService,
    ErrorService,
    LoggedInGuard
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
