import { Injectable } from '@angular/core';

@Injectable()
export class ErrorService {

    constructor() {}

    public getErrors(errors: any){
        let errorHandler = [];

        let found = Object.keys(errors).map(index => errors[index])

        for ( let key in found ) {
            errorHandler.push(found[key]);
        }

        return errorHandler;
    }

}