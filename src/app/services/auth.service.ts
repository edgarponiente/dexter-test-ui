import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { environment } from '../environment/env';

@Injectable()
export class AuthService {
  private loggedIn = false;
  private error: string;
  private token: string;
  public server: string;
  private headers: any;

  constructor(
    private router: Router,
    private http: Http) {
    let token = localStorage.getItem('token');

    this.server = environment.server;

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.headers = headers;
  }

  public login(password: string) {
    let headers = this.headers;

    return this.http
      .post( this.server + '/api/v1/password-check', JSON.stringify({password}), { headers })
      .map((res) => res.json())
      .catch((err) =>  {
          return Observable.throw(err.json());
      });
  }

  public logout() {
    this.loggedIn = false;
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  public isLoggedIn() {
    let token = localStorage.getItem('token');

    if (!token) {
      this.logout();
    }

    return this.loggedIn = true;
  }
}
