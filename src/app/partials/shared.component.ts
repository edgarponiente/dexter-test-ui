import {
    NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ErrorsComponent } from './errors/errors.component';
import { HeaderComponent } from './header/header.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        RouterModule,
        FormsModule
    ],
    declarations: [
        ErrorsComponent,
        HeaderComponent
    ],
    exports: [
        ErrorsComponent,
        HeaderComponent
    ]
})
export class SharedComponent { }
