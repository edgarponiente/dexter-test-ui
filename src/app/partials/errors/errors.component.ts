import {
  Component,
  Input,
  OnInit
} from '@angular/core';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'header'
  selector: 'errors',  // <errors></errors>
  // Our list of styles in our component. We may add more to compose many styles together
  styleUrls: [ ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  templateUrl: './errors.component.html'
})

export class ErrorsComponent implements OnInit {

  @Input() public error: any = [];

  constructor() {}

  public ngOnInit() {
    //
  }
}
