import {
    Component,
    OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';


@Component({
    /**
     * The selector is what angular internally uses
     * for `document.querySelectorAll(selector)` in our index.html
     * where, in this case, selector is the string 'home'.
     */
    // selector: 'home',  // <home></home>
    /**
     * We need to tell Angular's Dependency Injection which providers are in our app.
     */
    providers: [
        AuthService
    ],
    /**
     * Our list of styles in our component. We may add more to compose many styles together.
     */
    styles: [],

    selector: 'headers',
    /**
     * Every Angular template is first compiled by the browser before Angular runs it's compiler.
     */
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

    public errorMessage: boolean = false;
    private loggedIn = false;

    /**
     * TypeScript public modifiers
     */
    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    public ngOnInit() { }

    logout() {
        this.authService.logout();
        this.router.navigate(['/login']);
    }
}
